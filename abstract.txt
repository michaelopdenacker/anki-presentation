 * Présentateur : Michael Opdenacker

 * Titre : "Anki : n'oubliez plus ce que vous apprenez !"

 * Résumé : Avec l'outil libre Anki, créez des cartes mémoire pour
   retenir durablement ce que vous apprenez, que ça soit pour vos
   études, dans un domaine technique, pour apprendre une langue ou
   élargir votre culture générale. Et tout ça, en n'y passant pas plus
   de temps que nécessaire ! https://fr.wikipedia.org/wiki/Anki
